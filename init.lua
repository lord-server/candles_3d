local modname = minetest.get_current_modname()
local basename = ""
local S = minetest.get_translator(modname)
local creative_server = minetest.is_creative_enabled("")

local DEFAULT_LEADER_COLOR  = "white"
local DEFAULT_MIN_LUMINANCE = 6

local leader_color = minetest.settings:get("candles_3d-leader_color") or DEFAULT_LEADER_COLOR
local candle_count = minetest.settings:get("candles_3d-candles_per_node")
candle_count = tonumber(candle_count) or 5
candle_count = math.max(candle_count, 1)
candle_count = math.min(candle_count, 5)
candle_count = math.floor(candle_count)
local min_luminance = minetest.settings:get("candles_3d-min_luminance") or DEFAULT_MIN_LUMINANCE
local deprecated    = minetest.settings:get_bool("candles_3d-deprecated_nodes", false)

local candle_boxes = {
	{ type = "fixed", fixed = {-0.1, -0.5, -0.2, 0.25, -0.1, 0.15}},
	{ type = "fixed", fixed = { {-0.4, -0.5, 0, -0.1, 0.1, 0.25},
								{0, -0.5, -0.35, 0.35, -0.15, 0.0},}},
	{ type = "fixed", fixed = { {-0.45, -0.5, -0.05, -0.15, 0, 0.3},
								{-0.2, -0.5, -0.35, 0.1, 0.1, -0.1},
								{0.05, -0.5, 0.1, 0.4, 0, 0.4},}},
	{ type = "fixed", fixed = { {-0.3, -0.5, -0.35, -0.05, 0.1, -0.1},
								{0.05, -0.5, 0.05, 0.4, -0.1, 0.4},
								{0.15, -0.5, -0.4, 0.45, 0, -0.15},
								{-0.45, -0.5, -0.05, -0.15, 0.05, 0.3},
							}},
	{ type = "fixed", fixed = { {-0.25, -0.5, -0.45, 0.0, 0, -0.2},
								{0.2, -0.5, 0.2, 0.45, 0.1, 0.45},
								{-0.15, -0.5, -0.15, 0.25, -0.1, 0.25},
								{0.15, -0.5, -0.4, 0.45, 0, -0.15},
								{-0.45, -0.5, -0.05, -0.15, 0.05, 0.3},
							}},
}
local mcl = false
if minetest.get_modpath("mcl_fire") then
	basename = "mcl_candles"
	mcl = true
else
	basename = modname
end

local supported_colors = {}

if mcl then
	-- HACK: `mcl_dye.basecolors` & `mcl_dye.excolors` is not complete (not full) and have only 11 & 12 colors respectively.
	--       And `dyes` var with full list of colors (16 colors) is local var:
	--          https://git.minetest.land/MineClone2/MineClone2/src/commit/aa3b3421d54ba6ea2578f8d1786d969381baaf36/mods/ITEMS/mcl_dye/init.lua#L62
	--       Also no one var have no values for colorize png.
	--       So we cant just loop through any.
	supported_colors["white"]     = { description_en = "White", dye = "white" }
	supported_colors["lightgrey"] = { description_en = "Light Grey", dye = "grey" }
	supported_colors["grey"]      = { description_en = "Grey", dye = "dark_grey" }
	supported_colors["black"]     = { description_en = "Black", dye = "black" }
	supported_colors["violet"]    = { description_en = "Violet", dye = "violet" }
	supported_colors["blue"]      = { description_en = "Blue", dye = "blue" }
	--supported_colors["lightblue"] = { description_en = "Light Blue", dye = "lightblue" } -- too light; TODO:
	supported_colors["cyan"]      = { description_en = "Cyan", dye = "cyan" }
	supported_colors["green"]     = { description_en = "Green", dye = "dark_green" }
	supported_colors["lime"]      = { description_en = "Lime", dye = "green" }
	supported_colors["yellow"]    = { description_en = "Yellow", dye = "yellow" }
	supported_colors["brown"]     = { description_en = "Brown", dye = "brown" }
	supported_colors["orange"]    = { description_en = "Orange", dye = "orange" }
	supported_colors["red"]       = { description_en = "Red", dye = "red" }
	supported_colors["magenta"]   = { description_en = "Magenta", dye = "magenta" }
	supported_colors["pink"]      = { description_en = "Pink", dye = "pink" }

	for color_key, color_def in pairs(supported_colors) do
		supported_colors[color_key].dye = "mcl_dye:" .. color_def.dye
	end

elseif minetest.get_modpath("dye") then
	-- { {"dark_green", "Dark Green"}, ... }
	for _, color in ipairs(dye.dyes) do
		local color_key = color[1]:gsub("_","")
		local dye_item  = "dye:" .. color[1]
		supported_colors[color_key] = {
			description_en = color[2],
			dye = dye_item,
		}
	end
end

-- `leader_color` got from config can contains any string
-- Check if `leader_color` from config is right (exists), else use DEFAULT_LEADER_COLOR
(function()
	for color,_ in pairs(supported_colors) do
		if leader_color == color then return end
	end
	minetest.log("warning",
		"[ Candles 3D ]: unknown value '" .. leader_color .. "' for `candles_3d-leader_color` setting; use default: '" .. DEFAULT_LEADER_COLOR .. "'"
	)
	leader_color = DEFAULT_LEADER_COLOR
end)()


local is_candle = function(stack)
	return stack:get_name():sub(1,#basename+1) == basename .. ":"
end

local is_supported_color = function(strings)
	if not strings or ( strings[1] ~= "dye"
		and strings[1] ~= "mcl_dye" ) then
		return false
	end
	for color,_ in pairs(supported_colors) do
		if color == strings[2] then
			return true
		end
	end
	return false
end

local ignite = function(pos, igniter)
	local node = minetest.get_node(pos)
	minetest.swap_node(pos,
		{ param2 = node.param2,
		name = node.name:gsub("unlit_", "")
	})
end

local mcl_candle_ignite = function(player, pointed_thing)
	ignite(pointed_thing.under, player)
	return true
end

local candle_same_color = function(nodename, stackname)
	local nodecolor, stackcolor
	stackcolor = stackname:gsub(basename .. ":unlit_", "")
	stackcolor = stackcolor:gsub("_.*","")
	nodecolor = nodename:gsub(basename .. ":", "")
	nodecolor = nodecolor:gsub("unlit_", "")
	nodecolor = nodecolor:gsub("_.*","")
	return nodecolor == stackcolor
end

local register_candle = function(color, count)
	-- Initialize candle definition
	local def = {
		drawtype = "mesh",
		paramtype = "light",
		description = S(supported_colors[color].description_en .. " @1", S("Candle")),
		tiles = {
			"candles_3d_wax.png^[colorize:" .. color .. ":100",
			{name="candles_3d_flame.png", animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=3.0}},
		},
		mesh = "candles_3d_" .. count .. ".obj",
		walkable = false,
		floodable = true,
		groups = {
			dig_immediate = 3,
			attached_node = 1,
			not_in_creative_inventory = 1,
		},
		paramtype2 = "facedir",
		light_source = min_luminance + count - 1,
		use_texture_alpha = "blend",
		selection_box = candle_boxes[count],
		drop = basename .. ":unlit_" .. color .. "_1 " .. count,
	}
	if mcl then
		def.groups.dig_immediate = 2
	end

	local action = function(pos, node, clicker, itemstack, pointed_thing)
		local pn = clicker:get_player_name()
		if minetest.is_protected(pos, pn) then
			return
		end

		if mcl then
			pointed_thing = itemstack
			itemstack = clicker:get_wielded_item()
		end

		if itemstack:get_name() == "" then
			return
		end

		local unlit = ""
		if node.name:find("unlit_") then
			unlit = "unlit_"
		end

		-- When player holds a candle_3d increase candle counts in node
		if is_candle(itemstack) then
			if count < candle_count and	(
				candle_same_color(node.name, itemstack:get_name()) or
				minetest.is_creative_enabled(pn)
			) then
				minetest.swap_node(pos, { param2 = node.param2,
					name = basename .. ":" .. unlit .. color .. "_" .. count + 1 })
				if minetest.is_creative_enabled(pn) then
					return
				end
				itemstack:take_item()
				if mcl then
					clicker:set_wielded_item(itemstack)
				else
					return itemstack
				end
			end
			return
		end

		-- When player holds a dye change the color of the node
		local mi_split = itemstack:get_name():split(":")
		if mi_split then
			mi_split[2] = mi_split[2]:gsub("dark_","dark")
		end
		if is_supported_color(mi_split) and color ~= mi_split[2] then
			local replace_with = {
				param2 = node.param2,
				name = basename .. ":" .. unlit ..  mi_split[2] .. "_" .. count
			}

			if creative_server or minetest.check_player_privs(pn, "creative") then
				minetest.swap_node(pos, replace_with)
				return
			end

			if itemstack:get_count() < count then
				minetest.chat_send_player(pn, S("Not enough of dye items in hands!"))
				return
			end
			minetest.swap_node(pos, replace_with)
			itemstack:take_item(count)
			if mcl then
				clicker:set_wielded_item(itemstack)
			else
				return itemstack
			end
		end

		-- Light up candles with torches when not mineclone
		if not mcl and unlit ~= "" and itemstack:get_name() == "default:torch" then
			minetest.swap_node(pos, { param2 = node.param2,
				name = basename .. ":" .. color .. "_" .. count })
		end
	end

	if mcl then
		def.on_punch = action
	else
		def.on_rightclick = action
	end

	if count > 1 then
		-- Take candles from node one by one
		def.on_dig = function(pos, node, digger)
			local pn = digger:get_player_name()
			if minetest.is_protected(pos, pn) then
				return
			end
			local name = basename .. ":"
			if node.name:find("unlit_") then
				name = name .. "unlit_"
			end
			name = name .. color .. "_" .. count - 1

			minetest.swap_node(pos, { param2 = node.param2, name = name})
			if creative_server or
				minetest.check_player_privs(pn, "creative") then
				return
			end
			local stack = minetest.get_node_drops(node)[1]:split(" ")[1]
			minetest.handle_node_drops(pos, {stack}, digger)
		end
	end
	if not creative_server then
		def.on_flood = function(pos, oldnode, newnode)
			minetest.add_item(pos, ItemStack(def.drop))
			return false
		end
	end
	if minetest.get_modpath("screwdriver") then
		def.on_rotate = screwdriver.rotate_simple
	end

	-- Register illuminated candle node
	if mcl then
		minetest.register_node(":" .. basename .. ":" .. color .. "_" .. count, def)
	else
		local nn = basename .. ":" .. color .. "_" .. count
		minetest.register_node(nn, def)
		if deprecated then
			minetest.register_alias(basename .. ":candle_" .. color .. "_" .. count, nn)
		end
	end

	-- Adapt unlit candles definition
	def = table.copy(def)
	def.tiles = { "candles_3d_wax.png^[colorize:" .. color .. ":100", "candles_3d_wick.png" }
	def._tt_luminance = min_luminance .. ' - ' .. min_luminance + candle_count - 1
	def.light_source = nil

	-- Placeable node
	if count == 1 then
		def.groups.not_in_creative_inventory = nil
		def.on_construct = function(pos)
			local param2 = math.random(4) - 1 -- Always on floor
			local node = minetest.get_node(pos)
			minetest.swap_node(pos, { name = node.name, param2 = param2 })
		end
	end

	-- Register unlit node
	if mcl then
		def._on_ignite = mcl_candle_ignite
		minetest.register_node(":" .. basename .. ":unlit_" .. color .. "_" .. count, def)
	else
		def.on_ignite = ignite
		local nn = basename .. ":unlit_" .. color .. "_" .. count
		minetest.register_node(nn, def)
	end
end

local register_colored_crafts = function()
	for c1, _ in pairs(supported_colors) do
		local output_candle = basename .. ":unlit_" .. c1 .. "_1"
		local dye           = supported_colors[c1].dye
		if not minetest.registered_items[dye] then
			minetest.log("error", "Dye not found: " .. dye .. " [for " .. output_candle .. "]")
		end
		for c2, _ in pairs(supported_colors) do
			if c1 ~= c2 then
				local input_candle = basename .. ":unlit_" .. c2 .. "_1"
				minetest.register_craft({
					type   = "shapeless",
					output = output_candle,
					recipe = { input_candle, dye },
				})
			end
		end
	end
end

local register_leader_crafts = function()
	local leader_candle_recipes = 0

	if mcl then -- Recipe in mineclone

		minetest.register_craft({
			output = basename .. ":unlit_" .. leader_color .. "_1",
			recipe = {
				{"mcl_mobitems:string","", ""},
				{"mcl_honey:honeycomb",  "", "",},
				{"",  "", "",},
			}
		})
		leader_candle_recipes = leader_candle_recipes + 1

	elseif minetest.registered_items["farming:string"] then
		-- Recipe with bees + farming
		if minetest.get_modpath("bees") then
			minetest.register_craft({
				output = basename .. ":unlit_" .. leader_color .. "_1",
				recipe = {
					{'' , '', 'farming:string'},
					{'' ,  'bees:wax'    , '',},
					{'bees:wax' ,  ''    , '',},
				}
			})
			leader_candle_recipes = leader_candle_recipes + 1
		end
		-- Recipe with petz + farming
		if minetest.get_modpath("petz") then
			minetest.register_craft({
				type = "shapeless",
				output = basename .. ":unlit_" .. leader_color .. "_1",
				recipe = { "petz:beeswax_candle" }
			})
			leader_candle_recipes = leader_candle_recipes + 1
		end
		-- Recipe when only farming
		if leader_candle_recipes == 0 then
			minetest.register_craft({
				output = basename .. ":unlit_" .. leader_color .. "_1",
				recipe = {
					{'','farming:string','',},
					{'','group:leaves'  ,'',},
					{'','group:leaves'  ,'',},
				}
			})
			leader_candle_recipes = leader_candle_recipes + 1
		end
	end

	if leader_candle_recipes == 0 and not creative_server then
		minetest.log("warning", "[ Candles 3D ]: no recipe available: install bees and/or petz")
		minetest.log("warning", "[ Candles 3D ]: do it or use this mod via give privilege")
	end
end


for color,_ in pairs(supported_colors) do
	for i = 1, candle_count do
		register_candle(color, i)
	end
end

register_colored_crafts()

register_leader_crafts()
