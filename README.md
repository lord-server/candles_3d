Originally forked from [mazes_80/candles_3d](https://gitlab.com/mazes_80/candles_3d) 

### This mod adds colored candles.

**Supports:**
 - MineClone 2
 - MineTest_Game and other derivatives

# Features

 - Add up to five candles in one node using candles from stack
 - Remove them from world by digging candles in node one by one
 - Colorize candles in world with dyes
 - Craft colored candles with dyes
 - Light candles by torch or flint-and-steel

Note: In world you can only add candles with same dying to a candle node


# Dependencies

### For "Minetest game" | "default"
 - **Required:** `dye` (for candles nodes registration)
 - Optional:
   - `default` - for lighting candles (by `default:torch`)
   - `farming`, `bees`|`petz` - for craft recipes (otherwise only in creative)

Note: without `bees` or `petz` mods, leaves (`group:leaves`) and string (`farming:string`) will provide recipe for master candle.

### For "Mineclone 2"
 - **Required:** `mcl_dye` (for candles nodes registration)
 - Optional:
   - `mcl_fire` - for lighting candles (by `mcl_fire:flint_and_steel`)
   - `mcl_mobitems`, `mcl_honey` - for craft recipes (otherwise only in creative)

Note: all these mods already builtin in "MineCloe 2" game.

### **Additionally:**
 - `screwdriver` seems to be a helpful mod

----

## Technical info
### node names:
 - For MTG|defult: node names are prefixed with the mod name `candles_3d:`
 - For MCL2: node names are prefixed with `mcl_candles:`
